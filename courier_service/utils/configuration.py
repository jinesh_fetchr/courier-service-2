"""
Configuration constants to be used to provide changes to code at runtime
"""


COUNTRY_ADDRESSES = {
    "saudi_arabia": {
        "city": "Riyadh",
        "address": [
            "14, Haroon Al Rashid Road",
            "Near Haroon Rashid Crossing",
            "Al Safa Street"
        ]
    },
    "united_arab_emirates": {
        "city": "Dubai",
        "address": [
            "Warehouse 1",
            "The Box Secure Storage Compound",
            "Dubai Investment Park 1"
        ]
    },
    "egypt": {
        "city": "Cairo",
        "address": [
            "17 Mahmoud Bassiony Street",
            "Down Town, Cairo, Egypt"
        ]
    },
    "bahrain": {
        "city": "Manama",
        "address": [
            "Office 181, Building 3358",
            "Al Zamil Suite, Road 2845",
            "Block 428, Seef Area"
        ]
    }
}