"""
Holds values for validating reqs
"""
DEFAULT_CURRENCY = "USD"

DIMENSION_WEIGHT = 20

PACKAGE_WEIGHT = 40
PACKAGE_HEIGHT = 10
PACKAGE_WIDTH = 10
PACKAGE_DEPTH = 10

NEW_ORDER_REQUIRED_FIELDS = [
    ["provider_id"],
    ["language_code"],
    ["receiver_data", "name"],
    ["receiver_data", "city"],
    ["receiver_data", "phone"],
    ["receiver_data", "address"],
    ["receiver_data", "country"],
    ["receiver_data", "email"],
    ["sender_data", "name"],
    ["sender_data", "city"],
    ["sender_data", "phone"],
    ["sender_data", "address"],
    ["sender_data", "country"],
    ["sender_data", "email"],
    ["package_data", "tracking_id"],
    ["package_data", "so_number"],
    ["package_data", "product_description"],
    ["package_data", "package_type"],
    ["package_data", "weight"],
    ["package_data", "dimension_weight"],
    ["package_data", "width"],
    ["package_data", "height"],
    ["package_data", "depth"],
    ["package_data", "dimension_unit"],
    ["package_data", "insured_amount"],
    ["package_data", "is_dutiable"],
    ["package_data", "currency_code"],
]

ZERO = 0
ONE = 1
TWO = 2
THREE = 3
FOUR = 4
FIVE = 5
SIX = 6
SEVEN = 7
EIGHT = 8
NINE = 9


__all__ = [
    'DEFAULT_CURRENCY',
    'DIMENSION_WEIGHT',
    'NEW_ORDER_REQUIRED_FIELDS',
    'PACKAGE_DEPTH',
    'PACKAGE_HEIGHT',
    'PACKAGE_WEIGHT',
    'PACKAGE_WIDTH',
    'ZERO',
    'ONE',
    'TWO',
    'THREE',
    'FOUR',
    'FIVE',
    'SIX',
    'SEVEN',
    'EIGHT',
    'NINE'
]
