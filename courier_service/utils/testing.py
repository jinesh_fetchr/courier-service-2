from django.test.runner import DiscoverRunner
from django.test import TransactionTestCase

from mongoengine import connect
from mongoengine.connection import disconnect

from courier_service.settings.test import TEST_DBNAME

_running_test = False


class TestRunner(DiscoverRunner):
    def setup_databases(self, **kwangs):
        disconnect('courier_service_db')
        global _running_test
        _running_test = True

        db_name = 'testsuite'
        connect(db_name)
        print 'Creating test-database: ' + db_name

        return db_name

    def teardown_databases(self, db_name, **kwargs):
        db = connect(db_name)
        db.drop_database(db_name)
        print 'Dropping test-database: ' + db_name


class TestCase(TransactionTestCase):
    def _fixture_setup(self):
        pass
