"""
Messages constants hold messages that are used in more than one place
to provide a single point of change

application.constants.messages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Candidate for refactoring: move all constant messages here or export __all__
"""

BAD_CREDENTIALS = "The credentials provided are invalid"

DUPLICATE_ORDER_MESSAGE = "Duplicate order was tried to be saved by"

INVALID_ORDER_MESSAGE = "Invalid order was tried to be saved by "
INVALID_JSON_VALUES_MESSAGE = "Invalid order was tried to be saved by "

MISSING_REQUIRED_PARAMETER = "Sorry! All our code workers are busy, try later"

NEW_ORDER_CREATED_MESSAGE = "A new order was created successfully"
NO_JSON_FOUND = "Why are you sending empty json requests?"
NO_MICROSERVICE = "{} microservice is unavailable"

ONLY_POST = "Only POST methods allowed for this resource"

RABBIT_UNAVAILABLE = "Sorry! Our code is on strike, try later"
RABBIT_UNRESPONSIVE = "Sorry! All our code workers are busy, try later"

SIMILAR_ORDER_MESSAGE = "Similar order exists on the system"

USELESS_JSON_FOUND = "How do we say this, your JSON is useless, add appropriate keys"


__all__ = [
    'BAD_CREDENTIALS',
    'DUPLICATE_ORDER_MESSAGE',
    'INVALID_ORDER_MESSAGE',
    'INVALID_JSON_VALUES_MESSAGE',
    'MISSING_REQUIRED_PARAMETER',
    'NEW_ORDER_CREATED_MESSAGE',
    'NO_JSON_FOUND',
    'NO_MICROSERVICE',
    'ONLY_POST',
    'RABBIT_UNAVAILABLE',
    'RABBIT_UNRESPONSIVE',
    'SIMILAR_ORDER_MESSAGE',
    'USELESS_JSON_FOUND'
]
