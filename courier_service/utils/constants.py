"""
Repository for all the constants to be used in the application
"""
from collections import OrderedDict

from .messages import *
from .stamps import *
from wrappers import LookupDict

FIFTY = 50
HUNDRED = 100
THREE_HUNDRED = 300

FETCHR_INDEX = 0
PARTNER_INDEX = 1

NO_NUMBER = 0
NO_SPACE = 1
NO_SYMBOL = 2
NOT_EMPTY = 3
FILTER_COLLECTION = [
    NO_NUMBER,
    NO_SPACE,
    NO_SYMBOL,
    NOT_EMPTY
]


GOOD_BOY = 200
CREATED = 201   #: We have created the resource you wanted us to create
ACCEPTED = 202  #: Your request is with us, We have agreed to work on it when we can
BAD_REQUEST = 400   #: Your request is a crime against http
UNAUTHORIZED = 401  #: Are you sure you are authenticated
NOT_FOUND = 404
NOT_ALLOWED = 405   #: No no no, that is not part of our agreement
TIMEOUT = 408   #: We ran out of patience with your request
#: Request could not be completed due to a conflict with current state of the resource
HTTP_CONFLICT = 409
HEADER_PRECONDITION_FAILED = 412
UNSUPPORTED_MEDIA_TYPE = 415    #: Bad content type
UNPROCESSABLE_ENTITY = 422  #: Nice work but you are spitting garbage
SERVER_ERROR = 500  #: We tried our best but AMQP and RabbitMQ said no
NOT_IMPLEMENTED = 501   #: The action is not implemented on this server
BAD_GATEWAY = 502   #: Our partners are MIA
SERVICE_UNAVAILABLE = 503
GATEWAY_TIMEOUT = 504

GET = 'GET'
POST = 'POST'


INTERNAL_URLS = ['registrar', 'couriers', 'customers', 'data', 'orders', 'uploads']

MOCK_TTL = 60L
MOCK_TIME_LEFT = 10L

OAUTH = 'Basic'
OAUTH2 = 'Bearer'

TIME_ISO_FORMAT = "%Y-%m-%dT%H:%M:%S"
YYYY_MM_DD = "%Y-%m-%d"
YYYY_MM_DD_HH_MM_SS = "%Y-%m-%d %H:%M:%S"

REDIS_AUTH_TOKEN_PREFIX = 'credential:'
REDIS_SAVE_ERROR_MSG = 'Mismatch: Retrieved Name != Persisted Name ::: {} != {}'

VALID_JSON_DATA_TYPES = [list, dict, type(None)]


############################
#: AUTHENTICATION ERRORS 101

#: Codes
EXPIRED_TOKEN_CODE = 10100
INVALID_CREDENTIAL_CODE = 10101
INVALID_TOKEN_CODE = 10103
WRONG_CREDENTIAL_CODE = 10104

#: Objects
expired_token = LookupDict(
    code=EXPIRED_TOKEN_CODE, message='Your authentication token has expired')
invalid_credentials = LookupDict(
    code=INVALID_CREDENTIAL_CODE, message='Invalid credentials detected')
invalid_token = LookupDict(
    code=INVALID_TOKEN_CODE, message='Access to the requested resource is not authorized')
wrong_credentials = LookupDict(
    code=WRONG_CREDENTIAL_CODE, message='Credential could not be tied to a user account')

#: Container
authentication_errors = {
    "invalid_credentials": invalid_credentials,
    "expired_token": expired_token,
    "invalid_token": invalid_token,
    "wrong_credentials": wrong_credentials
}


########################
#: VALIDATION ERRORS 122

#: Codes
INVALID_HEADER_CODE = 12200
MISSING_HEADER_CODE = 12201
MISSING_PARAMETER_CODE = 12202
WRONG_PARAMETER_CODE = 12203
BAD_TOKEN_CODE = 12204
MISMATCHED_CODE = 12205
INVALID_URL_CODE = 12206
INVALID_REQUEST_CODE = 12207
MISSING_RESOURCE = 12208

#: Objects
invalid_header = LookupDict(
    code=INVALID_HEADER_CODE, message='Invalid header field found in request')
missing_header = LookupDict(
    code=MISSING_HEADER_CODE, message='You want us to answer you with no headers? tsk tsk tsk')
missing_parameter = LookupDict(
    code=MISSING_PARAMETER_CODE, message='Required parameter is missing')
wrong_parameter = LookupDict(
    code=WRONG_PARAMETER_CODE, message='Provided the wrong type for a required parameter')
bad_token = LookupDict(
    code=BAD_TOKEN_CODE, message='Bad token used in authentication scheme')
mismatched_parameters = LookupDict(
    code=MISMATCHED_CODE, message='Too many or too few parameters detected')
invalid_url = LookupDict(
    code=INVALID_URL_CODE, message='We tried really hard but that url not be resolved internally')
invalid_request = LookupDict(
    code=INVALID_REQUEST_CODE, message='That request is so bad it made us cry')
missing_resource = LookupDict(
    code=MISSING_RESOURCE, message='The resource you are looking does not exist on our system')

#: Containers
validation_errors = {
    "missing_parameter": missing_parameter,
    "missing_resource": missing_resource,
    "invalid_header": invalid_header,
    "missing_header": missing_header,
    "wrong_parameter": wrong_parameter,
    "bad_token": bad_token,
    "mismatched_parameters": mismatched_parameters,
    "invalid_url": invalid_url,
    "invalid_request": invalid_request
}


########################
#: INTERNAL ERRORS 109

#: Codes
INTERNAL_SERVER_ERROR_CODE = 10900
SERVICE_UNAVAILABLE_CODE = 10901
DUPLICATE_RESOURCE_CODE = 10902
SERVICE_NOT_FOUND_CODE = 10903
INVALID_RESPONSE = 10904
INVALID_VALUE = 10905
SUPERMAN_ERROR_CODE = 10909

#: Objects
internal_server_error = LookupDict(
    code=INTERNAL_SERVER_ERROR_CODE, message='Sorry! Something failed. Try again later')
service_unavailable = LookupDict(
    code=SERVICE_UNAVAILABLE_CODE, message='Service currently unavailable. Try again later')
duplicate_resource = LookupDict(
    code=DUPLICATE_RESOURCE_CODE, message='Resource with same details found in the system')
service_not_found_error = LookupDict(
    code=SERVICE_NOT_FOUND_CODE, message='Are you sure we provide this functionality')
invalid_response = LookupDict(
    code=INVALID_RESPONSE, message='We just fired someone for trying to send you bad response')
invalid_value_error = LookupDict(
    code=INVALID_VALUE, message="The value passed in is invalid")
superman_error = LookupDict(
    code=SUPERMAN_ERROR_CODE, message="Only Superman can do this")

internal_errors = {
    "service_unavailable_error": service_unavailable,
    "duplicate_resource_error": duplicate_resource,
    "internal_server_error": internal_server_error,
    "service_not_found_error": service_not_found_error,
    "invalid_response": invalid_response,
    "invalid_value": invalid_value_error,
    "superman_error": superman_error
}


###########################################################################
#: Create an ERROR_CODES container collection to hold all the error objects
ERROR_CODES = OrderedDict()

#: Instantiate the error objects unto the ERROR_CODES container collections
ERROR_CODES.authentication_errors = authentication_errors
ERROR_CODES.validation_errors = validation_errors
ERROR_CODES.internal_errors = internal_errors
