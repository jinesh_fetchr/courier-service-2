from rest_framework.views import APIView


class CourierView(APIView):

    def __init__(self):
        self.template = {
            "data": [],
            "success": True,
            "has_more": False,
            "version": 1.0,
            "code": 200
        }

    def set_success_response(self, data, *args, **kwargs):
        for k, v in kwargs.items():
            self.template[k] = v
        self.template['data'] = data

    def set_error_response(self, data, *args, **kwargs):
        for k, v in kwargs.items():
            self.template[k] = v
        self.template['errors'] = data
        self.template['code'] = 400
        self.template['success'] = False
        self.template.pop('data', None)
