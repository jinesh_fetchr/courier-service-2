"""

"""
A_FAKE_TOKEN = 'fakeToken'
A_FAKE_USER_ID = '5731b7e4c18caf211744bfcd'

FAKE_APP_NAME = 'fetchr web portal'
FAKE_APP_SECRET = 'fakeAppSecret'
FAKE_APP_TOKEN = 'fakeAppToken'
FAKE_AUTH2_HEADER_TOKEN_STRING = 'Bearer {}:{}'.format(A_FAKE_USER_ID, A_FAKE_TOKEN)

FAKE_BASIC_HEADER_TOKEN_STRING = 'Basic {}:{}'.format(A_FAKE_USER_ID, A_FAKE_TOKEN)

FAKE_TOKEN_STRING = 'fake_token'

FAKE_USER_ID = 'user_id'

FAKE_XAUTH2_HEADER_TOKEN_STRING = 'Basic UserID={}, Token={}, Extra=fakeExtra'.format(A_FAKE_USER_ID, A_FAKE_TOKEN)
