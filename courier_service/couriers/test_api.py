import json

from rest_framework.test import APIClient
from courier_service.utils.testing import TestCase


class CourierAPITestCase(TestCase):

    def setUp(self):
        self.client = APIClient()

    def test_get_courier_list(self):
        response = self.client.get('/couriers/')
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(content['success'], True)
        self.assertEqual(content['version'], 1.0)
        self.assertEqual(content['has_more'], False)

    def test_create_courier(self):
        order_input = {
            "credential": {
                "password": "test-courier",
                "username": "courier"
            },
            "integration_type": "valid",
            "provider_name": "dhl",
            "services": {
                "awb_order_url": "https://dhl.com/api/v3/orders/awb",
                "cancel_order_url": "https://dhl.com/api/v3/orders/<order_id>",
                "create_order_url": "https://dhl.com/api/v3/orders",
                "modify_order_url": "https://dhl.com/api/v3/orders/<order_id>"
            },
            "status_map": {
                "in_transit": [
                    "booked",
                    "pickup"
                ]
            }
        }
        response = self.client.post('/couriers/', order_input, format='json')
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(content['data']['id'])

    def test_invalid_courier_data(self):
        order_input = order_input = {
            "credential": {},
            "integration_type": "valid",
            "provider_name": "dhl",
            "services": {},
            "status_map": {}
        }
        response = self.client.post('/couriers/', order_input, format='json')
        content = json.loads(response.content)

        self.assertEqual(response.status_code, 400)
        output = {
            "services": {
                "cancel_order_url": [
                    "Missing data for required field."
                ],
                "awb_order_url": [
                    "Missing data for required field."
                ],
                "modify_order_url": [
                    "Missing data for required field."
                ],
                "create_order_url": [
                    "Missing data for required field."
                ]
            },
            "credential": {
                "username": [
                    "Missing data for required field."
                ],
                "password": [
                    "Missing data for required field."
                ]
            }
        }
        self.assertEqual(content['errors'], output)

    # def test_modify_order(self):
    #     response = self.client.post('/couriers/', order_input, format='json')
