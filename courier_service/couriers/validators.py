import marshmallow

from mongomodels import Country, City


class ModelCodeField(marshmallow.fields.Dict):

    def _deserialize(self, value, attr, data):
        model_inst = self.model.objects(code=value).first()
        if model_inst:
            return value
        raise marshmallow.exceptions.ValidationError(
            "Invalid input for {} field".format(self.model._class_name))


class CountryField(ModelCodeField):
    model = Country


class CityField(ModelCodeField):
    model = City
