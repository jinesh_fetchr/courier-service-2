from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns

from courier_service.couriers import views

urlpatterns = [
    url(r'^$', views.CourierList.as_view()),
    url(r'^$', views.CourierDetails.as_view()),
    url(r'^(?P<provider_id>[a-zA-Z]+)/orders/', include('courier_service.orders.urls')),
]

urlpatterns = format_suffix_patterns(urlpatterns)
