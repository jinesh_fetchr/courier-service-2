from __future__ import unicode_literals

import mongoengine


class CourierServices(mongoengine.EmbeddedDocument):
    create_order_url = mongoengine.URLField(required=True)
    cancel_order_url = mongoengine.URLField(required=True)
    awb_order_url = mongoengine.URLField(required=True)
    modify_order_url = mongoengine.URLField(required=True)


class Courier(mongoengine.Document):
    provider_name = mongoengine.StringField(required=True, unique=True)
    services = mongoengine.EmbeddedDocumentField(CourierServices, required=True)
    status_map = mongoengine.DictField(required=True)
    credential = mongoengine.DictField(required=True)
    integration_type = mongoengine.StringField(required=True)

    def __unicode__(self):
        return self.provider_name


class City(mongoengine.Document):
    """Not persisted to mongoengine"""
    name = mongoengine.StringField(required=True, unique=True)
    code = mongoengine.StringField(required=True, unique=True)
    country_name = mongoengine.StringField(required=True)
    country_code = mongoengine.StringField(required=True)
    postal_code = mongoengine.StringField()


class Country(mongoengine.Document):
    """Country model class

    Countries are the parent nodes of all address and delivery operations
    and processing

    >>> destination_country = Country(name="Nigeria", code="NG").save()
    >>> destination_country.name == "Nigeria"
    >>> destination_country.region_code = "AP"
    True

    >>> destination_country.delete()
    """
    __collection__ = 'destination_country'

    name = mongoengine.StringField(required=True, unique=True)
    # numeric_id = mongoengine.IntField(required=True)

    code = mongoengine.StringField(max_length=2, required=True, unique=True)
    currency_code = mongoengine.StringField(max_length=3)

    weight_unit = mongoengine.StringField(choices=("KG", "LB"))
    dimensional_unit = mongoengine.StringField(choices=("CM", "IN"))

    region_code = mongoengine.StringField()
    tags = mongoengine.ListField(mongoengine.StringField)

    uses_division = mongoengine.BooleanField(default=False)
    uses_postal_code = mongoengine.BooleanField(default=False)

    def get_country_code_from_possible_names(self, list_of_names):
        pass

    def get_country_code_from_numeric_id(self):
        pass

    def get_postal_code(self, city_name):
        """Get the postal code for countries that need it

        #: todo implement postal code lookup by city name later if required by business,
        i.e. populate the state coll.

        #: state = State.objects(country=self).first()
        #: city = state.cities.get(city_name)
        #: postal_code = city.get("postal_code")
        """
        if not self.code == "AM":
            return "00000"
        return 99999

    def __repr__(self):
        return "destination_country: %r, code: %r, region: %r" % \
            (self.name, self.code, self.region_code)
