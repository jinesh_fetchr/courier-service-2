from mongomodels import Courier

from django.contrib.auth.models import User, Group

from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets

from courier_service.utils.base_api import CourierView
from serializers import CourierSerializer, UserSerializer, GroupSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class CourierList(CourierView):

    def get(self, request, format=None):
        couriers = Courier.objects().as_pymongo()
        data = [dict(CourierSerializer().load(c)[0]) for c in couriers]
        self.set_success_response(data)
        return Response(self.template)

    def post(self, request, format=None):
        data = JSONParser().parse(request)
        courier, errors = CourierSerializer().load(data)

        if not errors:
            Courier.objects(
                provider_name=courier['provider_name']
            ).update_one(upsert=True, **courier)
            obj = Courier.objects(provider_name=courier['provider_name']).first()
            self.set_success_response({"id": str(obj.id)})
            return Response(self.template, status=status.HTTP_201_CREATED)

        self.set_error_response(errors)
        return Response(self.template, status=status.HTTP_400_BAD_REQUEST)


class CourierDetails(CourierView):

    def put(self, request, format=None):
        data = JSONParser().parse(request)
        courier, errors = CourierSerializer().load(data)
        if not errors:
            provider_name = courier.get('provider_name')
            Courier.objects(provider_name=provider_name).update_one(**courier)
            self.set_success_response({"provier_name": provider_name})
            return Response(self.template)

        self.set_error_response(errors)
        return Response(self.template, status=status.HTTP_400_BAD_REQUEST)
