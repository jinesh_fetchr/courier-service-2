import marshmallow
from django.contrib.auth.models import User, Group
from rest_framework import serializers


class ServicesSerializer(marshmallow.Schema):
    create_order_url = marshmallow.fields.Url(required=True)
    cancel_order_url = marshmallow.fields.Url(required=True)
    awb_order_url = marshmallow.fields.Url(required=True)
    modify_order_url = marshmallow.fields.Url(required=True)


class CredentialsSerializer(marshmallow.Schema):
    username = marshmallow.fields.Str(required=True)
    password = marshmallow.fields.Str(required=True)


class CourierSerializer(marshmallow.Schema):
    provider_name = marshmallow.fields.Str(required=True)
    services = marshmallow.fields.Nested(ServicesSerializer, required=True)
    status_map = marshmallow.fields.Dict(required=True)
    credential = marshmallow.fields.Nested(CredentialsSerializer, required=True)
    integration_type = marshmallow.fields.Str(required=True)


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')
