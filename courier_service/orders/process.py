try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET


class ProcessXMLResponse(object):
    """
    import requests

    from django.conf import settings
    from django.template.loader import render_to_string

    # create xml template
    context = order.get_validate_shipment_context()

    xml = render_to_string('xml/shipment_request.xml', dict(context))
    # call dhl order creation api
    if settings.DEBUG:
        xml_response = requests.post("http://xmlpitest-ea.dhl.com/XMLShippingServlet", xml)
    else:
        xml_response = requests.post("https://dhl.com/api/v3/orders", xml)

    # save shipment details - from response xml
    provider_response = ProcessXMLResponse(xml_response)
    order_info = provider_response.get_xml_response()
    Order.objects(
        essage_reference=order_info.get('message_reference')
    ).update_one(upsert=True, **data)
    """

    def __init__(self, xml_response):
        self.xml_response = ET.fromstring(xml_response._content)

    def lazy_xml_extraction(self, element_name):
        _xpath_query = ".//" + element_name
        if len(self.xml_response.findall(_xpath_query)):
            xml_element = self.xml_response.findall(_xpath_query).pop()
            return xml_element.text
        else:
            return ''

    def is_successful(self):
        value = self.xml_response.findall("*/ActionNote")
        return True if value else False

    def get_xml_response(self):
        if not self.is_successful():
            return {}

        awb_bar_code = self.lazy_xml_extraction("Barcodes/AWBBarCode")
        awb_number = self.lazy_xml_extraction("AirwayBillNumber")
        dhl_routing_barcode = self.lazy_xml_extraction("Barcodes/DHLRoutingBarCode")
        origin_destination_barcode = self.lazy_xml_extraction("Barcodes/OriginDestnBarcode")
        message_reference = self.lazy_xml_extraction("MessageReference")
        awb_image = self.lazy_xml_extraction("OutputImage")

        courier_metadata = dict(
            routing_barcode=dhl_routing_barcode,
            origin_destination_barcode=origin_destination_barcode,
        )

        return dict(
            message_reference=message_reference,
            airway_bill_number=awb_number,
            airway_bill_barcode=awb_bar_code,
            awb_image=awb_image,
            courier_data=courier_metadata)
