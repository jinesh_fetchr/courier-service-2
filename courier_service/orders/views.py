
from django.http import Http404

from rest_framework import status
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework_mongoengine.serializers import DocumentSerializer

from courier_service.utils.base_api import CourierView
from courier_service.utils.wrappers import LookupDict

from mongomodels import Order, save_order
from provider_picker import DHLProvider
from serializers import OrderSerializer


class OrderList(CourierView):
    """
    List orders or create a new order
    """

    def choose_best_provider(self, provider_id):
        """
        Must be done in courier models
        """
        courier_mapping = LookupDict(name="courier_class_mapping")

        setattr(courier_mapping, 'dhl', DHLProvider)
        setattr(courier_mapping, 'automatic', DHLProvider)

        return courier_mapping.provider_id

    def get(self, request, provider_id):
        self.set_success_response([])
        return Response(self.template)

    def post(self, request, provider_id, format=None):
        """
        1. ERP -> (send payload) -> Courier Service  [only if it's international delivery]
        2. Courier Service do schema validation
        3. DHL validation - [using library written by Jasim]
            1. DHL(username, password, account)
        4. If validation is successful push payload to queue
        5. Else inform ERP regarding failure -> END
        6. Read payload from queue -> DHL.create_order(payload) using celery
        """
        data = JSONParser().parse(request)
        # dump input data to database

        # input schema validation
        order_data, errors = OrderSerializer().load(data)

        if errors:
            self.set_error_response(errors)
            return Response(self.template, status=status.HTTP_400_BAD_REQUEST)

        # decide courier Provider class
        best_provider_class = self.choose_best_provider(provider_id)

        # provider data validation
        best_provider = best_provider_class(order_data)
        success = best_provider.validate()

        # if provider data validation is correct, create order asynchrnously
        if success:
            save_order(order_data)
            best_provider.create_order()
            resp_status = status.HTTP_201_CREATED
            self.set_success_response({"message": "Order creation initiated."})
        else:
            resp_status = status.HTTP_400_BAD_REQUEST
            self.set_error_response(self.template)
        return Response(self.template, status=resp_status)


class OrderDetail(CourierView):
    """
    Retreive, Update or Delete order
    """

    def get_object(self, tracking_id):
        order = Order.objects(tracking_id=tracking_id).first()
        if order:
            return order
        else:
            raise Http404

    def get(self, request, provider_id, tracking_id, format=None):
        order = self.get_object(tracking_id)
        order_data = DocumentSerializer(order)
        self.set_success_response(order_data)
        return Response(self.template)

    def delete(self, request, provider_id, tracking_id, format=None):
        order = self.get_object(tracking_id)
        order.delete()
        self.set_success_response({})
        return Response(self.template)

    def put(self, request, provider_id, tracking_id, format=None):
        data = JSONParser().parse(request)
        courier, errors = OrderSerializer().load(data)
        if not errors:
            Order.objects(tracking_id=tracking_id).update_one(**data)
            self.set_success_response({'tracking_id': tracking_id})
            return Response(self.template)

        self.set_error_response(errors)
        return Response(self.template, status=status.HTTP_400_BAD_REQUEST)
