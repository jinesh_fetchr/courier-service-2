Providers = {
    "DHL-SA": {
        "username": "DHL SA",
        "password": "abcd",
        "account": "1234"
    },
    "DHL-AE": {
        "username": "DHL AE",
        "password": "abcd",
        "account": "1234"
    }
}

ClientConfig = {
    "1163": {
        ("CN", "SA"): ("DHL-CN", "DHL", "FETCHR-SA")
    }
}

CountryMapping = {
    "SA": {"SA-RYD": "ADDR001"},
    "AE": {"AE-DXB": "ADDR002"}
}


class DHL():
    pass


class Provider(object):

    def __init__(self, order_data):
        self.name = "default"
        self.order_data = order_data

    def get_provider_info(self):
        raise NotImplementedError

    def validate(self, payload):
        raise NotImplementedError


class DHLProvider(Provider):

    def __init__(self, order_data):
        super().__init__(order_data)
        self.name = "dhl"
        # creating connection to dhl client
        self.provider_info = self.get_provider_info()
        self.dhl = DHL(
            self.provider_info.username,
            self.provider_info.password,
            self.provider_info.account
        )

    def get_provider_info(self):
        country = self.order_data['sender_data']['country']
        key = "{0}-{1}".format(self.name.upper(), country)
        return Providers.get(key)

    def validate(self, payload):
        self.dhl.validate(self.order_data)

    def create_order(self):
        self.dhl.create_order(self.order_data)
