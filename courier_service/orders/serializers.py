import marshmallow

from courier_service.couriers.validators import CountryField


class OrderPackageSerializer(marshmallow.Schema):
    tracking_id = marshmallow.fields.Str(required=True)
    so_number = marshmallow.fields.Str(required=True)
    erp_id = marshmallow.fields.Int(required=True)
    product_description = marshmallow.fields.Str(required=True)
    package_type = marshmallow.fields.Str(required=True)  # small
    weight = marshmallow.fields.Float(required=True)
    dimension_weight = marshmallow.fields.Float(required=True)
    width = marshmallow.fields.Float(required=True)
    height = marshmallow.fields.Float(required=True)
    depth = marshmallow.fields.Float(required=True)
    dimension_unit = marshmallow.fields.Str(required=True)
    insured_amount = marshmallow.fields.Float(required=True)
    is_dutiable = marshmallow.fields.Str(required=True)
    currency_code = marshmallow.fields.Str(required=True)
    no_of_pieces = marshmallow.fields.Int(required=True)
    pieces = marshmallow.fields.List(marshmallow.fields.Dict)


class OrderReceiverSerializer(marshmallow.Schema):
    name = marshmallow.fields.Str(required=True)
    city = marshmallow.fields.Str(required=True)
    location = marshmallow.fields.List(marshmallow.fields.Float, required=True)
    phone = marshmallow.fields.Str(required=True)
    address = marshmallow.fields.List(marshmallow.fields.Str, required=True)
    country = CountryField(required=True)
    email = marshmallow.fields.Email(required=True)


class OrderSenderSerializer(marshmallow.Schema):
    name = marshmallow.fields.Str(required=True)
    city = marshmallow.fields.Str(required=True)
    location = marshmallow.fields.List(marshmallow.fields.Float, required=True)
    phone = marshmallow.fields.Str(required=True)
    address = marshmallow.fields.List(marshmallow.fields.Str, required=True)
    country = CountryField(required=True)
    email = marshmallow.fields.Email(required=True)


class OrderSerializer(marshmallow.Schema):
    provider_id = marshmallow.fields.Str(required=True)
    language_code = marshmallow.fields.Str(required=True)
    pieces_enabled = marshmallow.fields.Str(required=True)  # y,
    receiver_data = marshmallow.fields.Nested(OrderReceiverSerializer, required=True)
    sender_data = marshmallow.fields.Nested(OrderSenderSerializer, required=True)
    package_data = marshmallow.fields.Nested(OrderPackageSerializer, required=True)
