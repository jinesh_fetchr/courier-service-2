from __future__ import absolute_import, unicode_literals

from celery import shared_task

from mongomodels import save_order


@shared_task
def create_order(payload):
    return save_order(payload)
