from __future__ import unicode_literals
import uuid

import mongoengine
from collections import OrderedDict
from datetime import datetime

from courier_service.couriers.mongomodels import Courier, Country
from courier_service.utils.configuration import COUNTRY_ADDRESSES

from courier_service.utils.constants import DEFAULT_CURRENCY, PACKAGE_WIDTH, PACKAGE_HEIGHT
from courier_service.utils.constants import PACKAGE_WEIGHT, PACKAGE_DEPTH, DIMENSION_WEIGHT
from courier_service.utils.constants import YYYY_MM_DD, TIME_ISO_FORMAT, ONE
from courier_service.utils.constants import FIFTY, HUNDRED, THREE_HUNDRED


class Sender(mongoengine.EmbeddedDocument):
    """
    Sender does not map to any table in the mongo mongoengine collection
    it exists solely for the purpose of schema mapping and control
    """
    name = mongoengine.StringField(required=True)
    city = mongoengine.StringField()
    location = mongoengine.ListField(mongoengine.StringField)
    phone = mongoengine.StringField()
    email = mongoengine.StringField()
    address_one = mongoengine.StringField()
    address_two = mongoengine.StringField()
    country = mongoengine.StringField()


class Receiver(mongoengine.EmbeddedDocument):
    """
    Receiver is the opposite of sender, this is also a mapping collection
    and does not persist to the mongoengine
    """

    #: This class exists should the sender and receiver schema and structure ever separate
    #: right now the sender is used for both scenarios

    name = mongoengine.StringField(required=True)
    city = mongoengine.StringField()
    location = mongoengine.ListField(mongoengine.StringField)
    phone = mongoengine.StringField()
    email = mongoengine.StringField()
    address_one = mongoengine.StringField()
    address_two = mongoengine.StringField()
    country = mongoengine.StringField()


class OrderStatus(mongoengine.EmbeddedDocument):
    status_offset = mongoengine.IntField()
    order_status = mongoengine.StringField(required=True)
    status_date = mongoengine.DateTimeField(default=datetime.utcnow())


class Order(mongoengine.Document):
    """Order class

    Represents an order to be created or created on the 3PL system

    >>> test_order = Order()
    >>> test_order_status = OrderStatus()

    >>> test_order.tracking_id = "56a"
    >>> test_order = test_order.save()
    >>> str(test_order.tracking_id)  # just in case unicode rears its beautiful head
    '56a'
    >>> test_order.delete()  # clean up after yourself
    """
    __collection__ = 'order'

    tracking_id = mongoengine.StringField(required=True, unique=True)
    airway_bill_number = mongoengine.StringField()
    shipment_date = mongoengine.DateTimeField(default=datetime.utcnow())
    destination_country = mongoengine.ReferenceField(Country)
    origin_country = mongoengine.ReferenceField(Country)

    last_mile = mongoengine.StringField(default="FETCHR")

    is_fetchr_pays = mongoengine.BooleanField(default=True)
    is_dutiable = mongoengine.BooleanField(default=False)
    acknowledged = mongoengine.BooleanField(default=False)

    message_reference = mongoengine.StringField(unique=True)
    awb_image = mongoengine.StringField()
    airway_bill_barcode = mongoengine.StringField()

    language_code = mongoengine.StringField(default="EN")
    region_code = mongoengine.StringField(default="AP")
    pieces = mongoengine.BooleanField(default=False)

    provider_id = mongoengine.ReferenceField(Courier)

    door_to_door = mongoengine.StringField(default="DD")
    delivery_type = mongoengine.StringField()

    so_number = mongoengine.StringField()
    partner_app_id = mongoengine.IntField()
    order_description = mongoengine.StringField()
    number_of_pieces = mongoengine.IntField()
    package_type = mongoengine.StringField()
    weight = mongoengine.IntField(default=45)
    weight_unit = mongoengine.StringField(default="K")
    dimension_weight = mongoengine.IntField()
    width = mongoengine.IntField()
    height = mongoengine.IntField()
    depth = mongoengine.IntField()
    dimension_unit = mongoengine.StringField()

    order_value = mongoengine.IntField(default=50)
    insured_amount = mongoengine.IntField(default=10)
    currency_code = mongoengine.StringField(max_length=3)

    courier_data = mongoengine.DictField()
    sender_data = mongoengine.DictField()
    receiver_data = mongoengine.DictField()

    order_status = mongoengine.EmbeddedDocumentField(OrderStatus)
    order_status_history = mongoengine.ListField(
        mongoengine.EmbeddedDocumentField(OrderStatus))
    permanently_failed = mongoengine.BooleanField(default=False)
    permanently_failed_reason = mongoengine.StringField(
        default="Check origin and destination values")

    created_at = mongoengine.DateTimeField(default=datetime.utcnow())
    updated_at = mongoengine.DateTimeField(default=datetime.utcnow())

    def format_shipment_date(self, date_format=None):
        """Return the shipment date in an easy to read string format"""
        date_format = date_format or YYYY_MM_DD
        return self.shipment_date.strftime(date_format)

    def get_awb(self):
        """
        Return the awb barcode for this order as a base sixty four encoded string

        :return:
        """
        if not self.permanently_failed:
            return self.awb
        return "No AWB for failed orders"

    def get_destination_country(self):
        """Get the destination_country that this order is going to"""
        pass

    def get_region_code_from_country_name(self, country_name):
        """Return a two digit region code based on the destination_country name"""
        pass

    def get_region_language(self):
        """Get the language for the region of this order"""
        return "EN"  #: TODO

    def get_validate_shipment_context(self):
        """
        Get a dictionary that can be rendered easily into the xml template for dispatching
        to the dhl endpoint

        :return context:    Contextable hash collection to be used for xml template
                            rendering
        """
        context = OrderedDict()
        receiver_data = OrderedDict()
        package_data = OrderedDict()
        sender_data = OrderedDict()

        # import pdb; pdb.set_trace()

        context["message_time"] = self.format_shipment_date(TIME_ISO_FORMAT)
        context["message_reference"] = self.message_reference

        context["region_code"] = self.destination_country.region_code
        context["region_language"] = self.language_code

        #: self.pieces will be false or a dictionary ala true
        #: MODIFIED as we need the origin dest barcode and this is a prereq for that
        context["pieces_enabled"] = "Y"  #: if self.pieces else "N"

        #: (Shipper, Receiver, Other)
        context["shipping_payment_type"] = "S" if self.is_fetchr_pays else "R"
        context["duty_payment_type"] = "R" if self.is_dutiable else "S"

        if self.last_mile_by_fetchr():
            context["consignee_company_name"] = self.last_mile
        else:
            context["consignee_company_name"] = self.sender_data.get("name")

        #: should get a list of strings
        receiver_data.address = self.receiver_data.get("address")

        receiver_data.city = self.receiver_data.get("city")
        receiver_data.postal_code = self.destination_country.get_postal_code(receiver_data.city)
        receiver_data.country_code = self.destination_country.code
        receiver_data.country_name = self.destination_country.name
        receiver_data.name = self.receiver_data.get("name")
        receiver_data.phone = self.receiver_data.get("phone")
        receiver_data.extension = ""
        receiver_data.fax_number = self.receiver_data.get("fax") or receiver_data.phone
        receiver_data.telex = ""
        receiver_data.email = self.receiver_data.get("email")

        #: reserved for future use should fetchr want to pursue this route
        context["commodity_code"] = "cc"
        context["commodity_name"] = self.order_description

        context["declared_value"] = self.order_value
        context["declared_currency"] = self.currency_code or \
            self.destination_country.currency_code or DEFAULT_CURRENCY

        context["shipper_ein"] = ""
        context["reference_id"] = self.tracking_id
        context["reference_type"] = "ST"  #: reserved for future modification should fetchr need it

        #: length of this collection is used as input in the xml template
        package_data.pieces = self.pieces if self.pieces else [{
            "piece_id": 1,
            "package_type": "CP",  #: Customer Provided
            "weight": self.weight,
            "width": self.width,
            "height": self.height,
            "depth": self.depth
        }]

        context["weight"] = self.weight
        context["weight_unit"] = self.origin_country.weight_unit[0]
        context["global_product_code"] = "P"  #: Delivery Type (Express Worldwide)
        context["local_product_code"] = "P"
        context["order_date"] = self.format_shipment_date()
        #: NOTE - Depends on commodity_name field above
        context["product_description"] = context["commodity_name"]
        context["door_to_door"] = self.door_to_door
        context["dimension_unit"] = "C"
        context["insured_amount"] = self.insured_amount
        context["package_type"] = "CP"
        context["is_dutiable"] = "Y" if self.is_dutiable else "N"
        context["currency_code"] = self.currency_code

        context["shipper_company_name"] = "FETCHR"
        context["address_lines"] = self.sender_data.get("address")
        context["city"] = self.sender_data.get("city")
        context["division_code"] = context["city"]  #: NOTE depends on the preceeding line
        context["country_code"] = self.origin_country.code
        context["country_name"] = self.origin_country.name

        sender_data.name = self.sender_data.get("name")
        sender_data.phone_number = self.sender_data.get("phone")
        sender_data.phone_extension = ""
        sender_data.fax_number = self.sender_data.get("fax") or sender_data.phone_number
        sender_data.telex = ""
        sender_data.email = self.sender_data.get("email")

        #: Warehousing, Packaging, Pickup and Delivery Notifications, Delivery Signature,
        # Return & Repeat Delivery
        #: See DHL Special Service Codes XLSX Sheet
        context["special_service_types"] = [3, "G", "J", "S", "T"]
        context["e_process_shipment"] = "N"

        context["receiver_data"] = receiver_data
        context["package_data"] = package_data
        context["sender_data"] = sender_data

        return context

    def last_mile_by_fetchr(self):
        """Return true or false depending on who does the last mile delivery"""
        return self.last_mile == "FETCHR"

    def populate_self_from_http_request(self, http_request, list_of_keys=None):
        """
        Populate the class with the fields from the json request if valid and provided otherwise
        provide the sensible defaults where possible

        :param http_request:        The http post request that initiated resource creation
                                    :type <type, 'flask.wrappers.Response'>

        :param list_of_keys:        The collection of keys to use if a pop style retrieval
                                    was to be implemented
                                    :type <type, 'list'>
        :return:
        """
        receiver_data = http_request.get("receiver_data")
        sender_data = http_request.get("sender_data")
        package_data = http_request.get("package_data")
        self.origin_country = sender_data.get("country")
        self.destination_country = receiver_data.get("country")

        coming_from = self.origin_country.name.lower()
        going_to = self.destination_country.name.lower()

        #: this block of code should be optimized, created on a lazy boy day,
        # and you can't tell me nothing
        if coming_from not in ["saudi arabia", "egypt", "bahrain", "united arab emirates"]:
            raise Exception("We do not yet operate from this location")
        if going_to not in ["saudi arabia", "egypt", "bahrain", "united arab emirates"]:
            raise Exception("We do not yet operate to this location")

        going_to_address = self.__get_default_fetchr_address(going_to)
        coming_from_address = self.__get_default_fetchr_address(coming_from)

        receiver_data["address"] = going_to_address["address"]
        receiver_data["city"] = going_to_address["city"]
        sender_data["city"] = coming_from_address["city"]

        self.sender_data = sender_data
        self.receiver_data = receiver_data

        self.language_code = http_request.get("language_code") or "EN"
        self.pieces = package_data.get("pieces") if http_request.get("pieces_enabled") else False

        self.erp_id = package_data["erp_id"]
        self.tracking_id = package_data["tracking_id"]
        self.so_number = package_data["so_number"]
        self.order_description = package_data["product_description"]

        #: note to future maintainers, do not unbundle these shortcut names
        # from their direct usage below
        w = int(package_data["width"])
        h = int(package_data.get("height"))
        d = int(package_data.get("depth")) or HUNDRED
        dw = int(package_data.get("dimension_weight")) or HUNDRED
        ov = package_data.get("order_value") or HUNDRED
        ia = int(package_data.get("insured_amount")) or HUNDRED

        self.order_value = ov if (ov < HUNDRED and ov > FIFTY) else HUNDRED
        self.weight = int(package_data.get("weight")) or PACKAGE_WEIGHT
        self.height = h if h < THREE_HUNDRED else PACKAGE_HEIGHT
        self.width = w if w < THREE_HUNDRED else PACKAGE_WIDTH
        self.depth = d if d < THREE_HUNDRED else PACKAGE_DEPTH
        self.dimension_weight = dw if dw < FIFTY else DIMENSION_WEIGHT
        #: DHL requires insured_amount to be < order_value
        self.insured_amount = ia if ia < ov else ia - ONE

        self.package_type = package_data.get("package_type")
        self.dimension_unit = package_data.get("dimension_unit")
        self.is_dutiable = True if package_data.get("is_dutiable") == "Y" else False
        self.currency_code = package_data.get("currency_code") or "AED"

    def __get_default_fetchr_address(self, country_name):
        _space = " "
        _underscore = "_"
        country_name = country_name.replace(_space, _underscore)
        going_to_country = COUNTRY_ADDRESSES.get(country_name)
        return going_to_country


def save_order(data):
    tracking_id = data.get("package_data").get("tracking_id")

    order = Order.objects(tracking_id=tracking_id).first()
    if order:
        return order

    order_status = OrderStatus(
        status_offset=1,
        order_status="Acknowledged",
        status_date=datetime.utcnow()
    )

    new_order = Order()
    new_order.order_status = order_status
    new_order.message_reference = uuid.uuid4().hex
    new_order.order_status_history.append(order_status)
    new_order.populate_self_from_http_request(data)
    new_order.save()

    return new_order
