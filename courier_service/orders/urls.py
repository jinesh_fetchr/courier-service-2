from django.conf.urls import url
from courier_service.orders import views

urlpatterns = [
    url(r'^$', views.OrderList.as_view()),
    url(r'^(?P<tracking_id>[a-zA-Z0-9]+)/$', views.OrderDetail.as_view()),
]
