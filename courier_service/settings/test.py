from .base import *

import mongoengine
TEST_DBNAME = 'testsuite'
mongoengine.connect(TEST_DBNAME)

TEST_RUNNER = 'courier_service.utils.testing.TestRunner'
